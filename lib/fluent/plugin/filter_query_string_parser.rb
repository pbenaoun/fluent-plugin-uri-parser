module Fluent
  module Plugin
    class QueryStringParserFilter < Filter
      Fluent::Plugin.register_filter("query_string_parser", self)

      config_param :out_key_path, :string, default: 'path'
      config_param :out_key_query, :string, default: 'params'
      config_param :out_key_fragment, :string, default: 'fragment'
      config_param :key_name, :string
      config_param :hash_value_field, :string, default: nil
      config_param :inject_key_prefix, :string, default: nil
      config_param :suppress_parse_error_log, :bool, default: false
      config_param :ignore_key_not_exist, :bool, default: false
      config_param :emit_invalid_record_to_error, :bool, default: true
      config_param :multi_value_params, :bool, default: false

      def initialize
        super
        require "addressable/uri"
      end

      def cleaner_prefix(line)
        line.gsub(Regexp.new('^.*(\?)'), '').strip
      end
      
      def valid_url(line)
        if line.match(Regexp.new('(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})'))
          true
        elsif line.nil?
          false
        else
          false
        end
      end

      def filter(tag, time, record)
        raw_value = record[@key_name]

        if raw_value.nil?
          if @emit_invalid_record_to_error
            router.emit_error_event(tag, time, record, ArgumentError.new("#{@key_name} does not exist"))
          end
          return @ignore_key_not_exist ? nil : record
        end

        begin
          if valid_url(raw_value)
            uri = Addressable::URI.parse(raw_value)
            values = {}
            values[@out_key_path] = uri.path if @out_key_path
            values[@out_key_query] = uri.query if @out_key_query
            values[@out_key_fragment] = uri.fragment if @out_key_fragment
            values.reject! {|_, v| v.nil? } if @ignore_nil
            unless values[@out_key_query].nil?
              params = Addressable::URI.form_unencode(cleaner_prefix(values[@out_key_query]))

              unless params.nil?
                if @multi_value_params
                  values = Hash.new {|h,k| h[k] = [] }
                  params.each{|pair| values[pair[0]].push(pair[1])}
                else
                  values = Hash[params]
                end
                if @inject_key_prefix
                  values = Hash[values.map{|k,v| [ @inject_key_prefix + k, v ]}]
                end
                r = @hash_value_field ? { @hash_value_field => values } : values
                record = record.merge(r)
              end
            end

          else
            log.warn "url not valid: #{raw_value}"
          end
        rescue => e
            log.warn "parse failed #{e.message}" unless @suppress_parse_error_log
        end
        record
      end
    end
  end
end
